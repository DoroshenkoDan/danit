import React, { useEffect, useState } from "react";
import Favorite from "../../Components/Favorite/index.js"
export default function () {
    const [favItems, setFavItems] = useState([]);
    const [favIds, setFavIds] = useState([]);
    const [filteredItems, setFilteredItems] = useState([]);

    useEffect(() => {
        fetch(`${process.env.PUBLIC_URL}/products.json`)
            .then((response) => response.json())
            .then((result) => {
                setFavItems(result);
            });
    }, []);

    useEffect(() => {
        const storedFavIds = JSON.parse(localStorage.getItem("Fav")) || [];
        setFavIds(storedFavIds);
    }, []);

    useEffect(() => {
        const filtered = favItems.filter(item => favIds.includes(item.id));
        setFilteredItems(filtered);
    }, [favItems, favIds]);



    return (
        <>
            {filteredItems.map((card) => {
                return <Favorite src={card.src} model={card.model} text={card.text} price={card.price} favId={card.id} />

            })}
        </>
    );
}
