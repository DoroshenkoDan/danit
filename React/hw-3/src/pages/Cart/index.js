import CartCard from "../../Components/CartCard";
import "../../Components/CartCard/style.css"
import React, { useEffect, useState, useCallback } from "react";
import "./style.css"
export default function () {
    let [cartArray, setCartArray] = useState([])

    useEffect(() => {
        let arrayFromLocalStorage = JSON.parse(localStorage.getItem("Cart"));
        if (arrayFromLocalStorage) {
            setCartArray(arrayFromLocalStorage)
        } else {
            console.log("Nichego nety");
        }

    }, [])


    const removeFromCart = useCallback((id) => {
        let updatedCartArray = cartArray.filter(item => item.id !== id);
        setCartArray(updatedCartArray); // Обновляем состояние
        localStorage.setItem("Cart", JSON.stringify(updatedCartArray)); // Используем актуальное значение для сохранения в localStorage
    }, [cartArray]);



    return (
        <div className="container">
            {cartArray.map((card) => {
                return <CartCard key={card.id} src={card.src} model={card.model} price={card.price} removeFromCart={() => { removeFromCart(card.id) }} />

            })}
        </div>



    );
}

