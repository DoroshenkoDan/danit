import { Outlet, NavLink } from "react-router-dom";
import "./style.css"
export default function () {
    return (
        <>
            <header className="header">

                <div className="navigation-header-nav">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <NavLink className={({ isActive }) => isActive ? 'nav-link active' : 'nav-link'} aria-current="page" to="/">Home</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/Cart/">Cart</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/Favorite/">Favorite</NavLink>
                        </li>
                    </ul>
                </div>
            </header>
            <main className="container-main">
                <Outlet></Outlet>
            </main>
        </>
    )
}

