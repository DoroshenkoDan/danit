import React, { useEffect, useState, useCallback } from "react";
import Card from "../Card";
import "./style.css"
import Modal from "../Modal"
import PreLoader from "../../Components/Preloader";
export default function () {
    const [isLoaded, setIsLoaded] = useState(false);
    const [cardsArray, setCardsArray] = useState([]);

    //---Cart---
    const [cartGoods, setCartGoods] = useState([]);

    //------
    useEffect(() => {
        fetch("./products.json")
            .then((response) => response.json())
            .then((result) => {
                setTimeout(() => {
                    setIsLoaded(true);
                }, 800);
                setCardsArray(result);
            });
    }, []);





    let [modalInfo, setModalInfo] = useState([])
    const [modalOpen, setModalOpen] = useState(false)

    const ModalInfo = useCallback((id) => {
        const obj = cardsArray.find(item => item.id === id);
        setModalInfo([obj])
        setModalOpen(true)
    }, [cardsArray])


    const addToCart = useCallback((array) => {
        setCartGoods(prevCartGoods => [...prevCartGoods, ...array]);
        setModalOpen(false)
    }, [cardsArray]);


    useEffect(() => {
        if (cartGoods.length === 0) {
            const storedCart = localStorage.getItem("Cart");
            if (storedCart) {
                setCartGoods(JSON.parse(storedCart));
            }
        } else {
            localStorage.setItem("Cart", JSON.stringify(cartGoods));
        }
    }, [modalOpen]);

    const addToFav = useCallback(() => {
    }, [])





    if (!isLoaded) {
        return <PreLoader />;
    }

    return (<div className="cards-container">

        {cardsArray.map((card) => {
            return <Card key={card.id} src={card.src} model={card.model} text={card.text} ModalInfo={() => ModalInfo(card.id)} favId={card.id} />

        })}

        {modalOpen && <Modal model={modalInfo[0].model} saveToCart={() => { addToCart(modalInfo) }} closeModal={() => { setModalOpen(false) }} />}
    </div>);
}
