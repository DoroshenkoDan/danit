import "../../Components/Card/style.css"
import { BiSolidHeart } from "react-icons/bi";
import React, { useEffect, useState } from "react";

export default function (props) {
    const [isClicked, setIsClicked] = useState(false);

    const toggleButton = (id) => {
        setIsClicked(!isClicked);
        updateLocalStorage(id);
    };

    const updateLocalStorage = (id) => {
        const storedFavArray = JSON.parse(localStorage.getItem("Fav")) || [];

        if (storedFavArray.includes(id)) {
            const updatedFavArray = storedFavArray.filter(item => item !== id);
            localStorage.setItem("Fav", JSON.stringify(updatedFavArray));
        } else {
            const updatedFavArray = [...storedFavArray, id];
            localStorage.setItem("Fav", JSON.stringify(updatedFavArray));
        }
    };

    const isFavorited = JSON.parse(localStorage.getItem("Fav"))?.includes(props.favId) || false;
    return (
        <div className="card">
            <div className="heart-container">
                <BiSolidHeart
                    onClick={() => {
                        toggleButton(props.favId);
                    }}
                    className={`heart-icon ${isFavorited ? "heart-icon-clicked" : ""}`}
                />
            </div>
            <div className="card-img-circle">
                <img src={`/${process.env.PUBLIC_URL}${props.src}`} />
            </div>

            <h1 className="card-model">{props.model}</h1>
            <p className="card-description">{props.text}</p>
            <p className="card-description">{props.price}</p>
        </div>
    )
}


