import React, { useState } from "react";
import "./style.css";
import { BiSolidHeart } from "react-icons/bi";
import { useSelector, useDispatch } from 'react-redux';
import { removeFromFavorite, addToFavorite, selected, unselected, changeStatus } from "../../store/Reducers/favReducer.js";
export default function (props) {

    const dispatch = useDispatch();
    const Favoritelist = useSelector(state => state.store.favorite.favorite)





    const toggleButton = (id) => {
        if (Favoritelist.includes(id)) {
            dispatch(removeFromFavorite(id));
        } else {
            dispatch(addToFavorite(id));
        }
    };



    const isFavorited = Favoritelist.includes(props.favId) || false;

    return (
        <>
            <div className="card">
                <div className="heart-container">
                    <BiSolidHeart
                        onClick={() => toggleButton(props.favId)}
                        className={`heart-icon ${isFavorited ? "heart-icon-clicked" : ""}`}
                    />
                </div>

                <div className="card-img-circle">
                    <img src={process.env.PUBLIC_URL + props.src} />
                </div>

                <h1 className="card-model">{props.model}</h1>
                <p className="card-description">{props.text}</p>
                <button className="card-btn-addToCart" onClick={props.ModalInfo}>Add to Cart</button>
            </div>
        </>
    );
}
