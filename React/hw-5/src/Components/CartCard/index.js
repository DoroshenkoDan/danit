import "./style.css"

export default function (props) {
    return (
        <div className="cart-card">
            <div className="cart-card-img-circle">
                <img src={`/${process.env.PUBLIC_URL}${props.src}`} />

            </div>
            <div className="cart-card-description">
                <p>{props.model}</p>
                <p>Price: {props.price}</p>
                <p>Count: {props.count}</p>

                <button className="cart-card-order-now">Order Now!</button>
                <button onClick={props.removeFromCart} className="cart-card-remove">Remove</button>
            </div>

        </div>
    )
}


