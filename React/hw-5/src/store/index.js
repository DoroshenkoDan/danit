import { Provider } from "react-redux";
import { configureStore, combineReducers } from "@reduxjs/toolkit";


//Імпорт модулів, необхідних для зберігання стейджу між перезавантаженнями
import storage from 'redux-persist/lib/storage'
import { persistReducer, persistStore } from "redux-persist";
import { PersistGate } from "redux-persist/integration/react";

//Импорт редюсерів
import productsReducer from "./Reducers/productsSlice"
import modalState from "./Reducers/modalReducer"
import cartReducer from "./Reducers/cartReducer";
import favReducer from "./Reducers/favReducer";
//Об'єднання редюсерів


const storeReducers = combineReducers({
    cart: cartReducer,
    favorite: favReducer,
});

const persistedReducers = persistReducer({ key: 'root', storage }, storeReducers);

const store = configureStore({
    reducer: {
        store: persistedReducers,
        modal: modalState,
        products: productsReducer,
    }
});

const persistedStore = persistStore(store);


export default function Store(props) {

    return (
        <Provider store={store}>
            <PersistGate persistor={persistedStore}>
                {props.children}
            </PersistGate>
        </Provider>
    )
}