import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    cart: [],
    status: false,
}

export const cartReducer = createSlice({
    name: 'cart',
    initialState,
    reducers: {
        addToCart(state, action) {
            const [itemToAdd] = action.payload;
            const itemInCartIndex = state.cart.findIndex(item => item.id === itemToAdd.id);

            if (itemInCartIndex !== -1) {
                state.cart[itemInCartIndex].count += 1;
            } else {
                state.cart.push({ ...itemToAdd, count: 1 });
            }
        },
        removeFromCart(state, action) {
            const idToRemove = action.payload;
            const itemIndex = state.cart.findIndex(item => item.id === idToRemove);

            if (itemIndex !== -1) {
                if (state.cart[itemIndex].count > 1) {
                    state.cart[itemIndex].count -= 1;
                } else {
                    state.cart.splice(itemIndex, 1);
                }
            }
        },
        clearCart(state, action) {
            state.cart = [];
        },
        openCartForm(state) {
            state.status = true;
        },
        closeCartForm(state) {
            state.status = false;
        },
    }
})


export default cartReducer.reducer
export const { addToCart, removeFromCart, clearCart, openCartForm, closeCartForm } = cartReducer.actions;

