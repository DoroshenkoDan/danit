import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    status: false,
}

export const modalState = createSlice({
    name: 'counter',
    initialState,
    reducers: {
        modalOpen(state, action) {
            state.status = true
        },
        modalClose(state, action) {
            state.status = false
        }
    }
})

export default modalState.reducer
export const { modalOpen, modalClose } = modalState.actions;

