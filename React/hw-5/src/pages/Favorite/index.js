import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from 'react-redux';
import Favorite from "../../Components/FavoriteCard/index.js"
import { fetchProducts } from "../../store/Reducers/productsSlice";
import PreLoader from "../../Components/Preloader";
export default function () {
    const [filteredItems, setFilteredItems] = useState([]);
    const dispatch = useDispatch();
    const favoriteList = useSelector(state => state.store.favorite.favorite);
    const list = useSelector(state => state.products.data);



    const [isLoaded, setIsLoaded] = useState(false);

    useEffect(() => {
        dispatch(fetchProducts())
        setTimeout(() => {
            setIsLoaded(true);
        }, 800);
    }, []);


    useEffect(() => {
        if (list && favoriteList.length > 0) {
            const filtered = list.filter(item => favoriteList.includes(item.id));
            setFilteredItems(filtered);
        }
    }, [favoriteList, list]);

    if (!isLoaded) {
        return <PreLoader />;
    }

    if (favoriteList.length === 0) {
        return <p>No Items</p>;
    }


    if (filteredItems.length === 0) {
        return <p>No favorite items found</p>;
    }

    return (
        <>
            {filteredItems.map((card) => {
                return <Favorite src={card.src} model={card.model} text={card.text} price={card.price} favId={card.id} />
            })}
        </>
    );
}
