import React from "react";
import "./App.css";
import Router from './routers/Router'
import Store from "./store";


function App() {

    return (
        <Store>
            <Router />
        </Store>
    );
}

export default App;
