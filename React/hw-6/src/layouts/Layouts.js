import { Outlet, NavLink } from "react-router-dom";
import "./style.css"
import { fetchProducts } from "../store/Reducers/productsSlice.js";
import { useSelector, useDispatch } from 'react-redux';
import { useEffect } from "react";
import SwitcherCardDisplay from "../Components/SwitcherCardDisplay";
import BeforeHeader from "../Components/BeforeHeader/index.js";
export default function () {

    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(fetchProducts())
    }, []);


    return (
        <>
            <BeforeHeader />
            <header className="header">

                <div className="navigation-header-nav">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <NavLink className={({ isActive }) => isActive ? 'nav-link active' : 'nav-link'} aria-current="page" to="/">Home</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/Cart/">Cart</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/Favorite/">Favorite</NavLink>
                        </li>
                    </ul>
                </div>
            </header>
            <main className="container-main">
                <Outlet></Outlet>
            </main>
        </>
    )
}

