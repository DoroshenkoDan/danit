import Cardlist from "../../Components/Cardlist";
import SwitcherCardDisplay from "../../Components/SwitcherCardDisplay";
import { createContext, useState } from "react";

export const displayProductsContext = createContext()

export default function () {

    let [displayStyle, setDisplayStyle] = useState(true)

    return (
        <displayProductsContext.Provider value={{ displayStyle, setDisplayStyle }}>
            <SwitcherCardDisplay />
            <Cardlist />
        </displayProductsContext.Provider>



    );
}

