import CartCard from "../../Components/CartCard";
import "../../Components/CartCard/style.css"
import "./style.css"
import { useSelector, useDispatch } from 'react-redux';
import { removeFromCart } from "../../store/Reducers/cartReducer";
import React, { useEffect, useState } from "react";
import { openCartForm } from "../../store/Reducers/cartReducer";
import OrderForm from "../../Components/CartForm";
export default function () {

    const dispatch = useDispatch();
    const cartList = useSelector(state => state.store.cart.cart)
    let cartFormStatus = useSelector(state => state.store.cart.status)


    if (cartList.length === 0) {
        return <p>No Items</p>;
    }


    return (<>
        <div className="container">

            <button onClick={() => dispatch(openCartForm())} className="cart-order-all-btn">Order All!</button>


            {cartList.map((card) => {
                return <CartCard key={card.id} src={card.src} model={card.model} price={card.price} count={card.count} removeFromCart={() => { dispatch(removeFromCart(card.id)) }} />

            })}



        </div>

        {cartFormStatus && < OrderForm />}



    </>
    );
}

