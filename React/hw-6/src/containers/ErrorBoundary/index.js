import React from "react";

export default class ErrorBoundary extends React.PureComponent {

    state = {hasError: false};

    componentDidCatch(error, errorInfo) {
        console.log('Error:', error);
        console.log('Error info:', errorInfo);
    }

    static getDerivedStateFromError(error) {
        return {hasError: true};
    }

    render() {
        if(this.state.hasError) {
            return <div><h1>{this.props.message}</h1></div>
        }

        return this.props.children;
    }
}
