import { addToCart, removeFromCart, clearCart, openCartForm, closeCartForm } from '../Reducers/cartReducer.js';
import cartReducer from '../Reducers/cartReducer.js';

describe('cartReducer', () => {
    it('should add an item to the cart', () => {
        const initialState = {
            cart: [],
            status: false,
        };

        const itemToAdd = { id: 1, name: 'Product 1', price: 10 };

        const newState = cartReducer(initialState, addToCart([itemToAdd]));

        expect(newState.cart).toHaveLength(1);
        expect(newState.cart[0]).toEqual({ ...itemToAdd, count: 1 });
    });

    it('should increment the count when adding the same item to the cart', () => {
        const initialState = {
            cart: [{ id: 1, name: 'Product 1', price: 10, count: 1 }],
            status: false,
        };

        const itemToAdd = { id: 1, name: 'Product 1', price: 10 };

        const newState = cartReducer(initialState, addToCart([itemToAdd]));

        expect(newState.cart).toHaveLength(1);
        expect(newState.cart[0]).toEqual({ ...itemToAdd, count: 2 });
    });

    it('should remove an item from the cart', () => {
        const initialState = {
            cart: [{ id: 1, name: 'Product 1', price: 10, count: 1 }],
            status: false,
        };

        const itemIdToRemove = 1;

        const newState = cartReducer(initialState, removeFromCart(itemIdToRemove));

        expect(newState.cart).toHaveLength(0);
    });

    it('should decrement the count when removing an item with count > 1 from the cart', () => {
        const initialState = {
            cart: [{ id: 1, name: 'Product 1', price: 10, count: 2 }],
            status: false,
        };

        const itemIdToRemove = 1;

        const newState = cartReducer(initialState, removeFromCart(itemIdToRemove));

        expect(newState.cart).toHaveLength(1);
        expect(newState.cart[0]).toEqual({ id: 1, name: 'Product 1', price: 10, count: 1 });
    });

    it('should remove an item from the cart when count is 1', () => {
        const initialState = {
            cart: [{ id: 1, name: 'Product 1', price: 10, count: 1 }],
            status: false,
        };

        const itemIdToRemove = 1;

        const newState = cartReducer(initialState, removeFromCart(itemIdToRemove));

        expect(newState.cart).toHaveLength(0);
    });

    it('should clear the cart', () => {
        const initialState = {
            cart: [{ id: 1, name: 'Product 1', price: 10, count: 1 }],
            status: false,
        };

        const newState = cartReducer(initialState, clearCart());

        expect(newState.cart).toHaveLength(0);
    });

    it('should open the cart form', () => {
        const initialState = {
            cart: [],
            status: false,
        };

        const newState = cartReducer(initialState, openCartForm());

        expect(newState.status).toEqual(true);
    });

    it('should close the cart form', () => {
        const initialState = {
            cart: [],
            status: true,
        };

        const newState = cartReducer(initialState, closeCartForm());

        expect(newState.status).toEqual(false);
    });
});
