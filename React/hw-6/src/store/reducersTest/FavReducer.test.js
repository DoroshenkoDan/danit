import favoriteReducer, { addToFavorite, removeFromFavorite, selected, unselected, changeStatus } from '../Reducers/favReducer.js';

describe('favoriteReducer', () => {
    it('should add a number to favorites', () => {
        const initialState = {
            favorite: [],
            status: false,
        };

        const numberToAdd = 42;
        const newState = favoriteReducer(initialState, addToFavorite(numberToAdd));

        expect(newState.favorite).toEqual([numberToAdd]);
    });

    it('should remove a number from favorites', () => {
        const initialState = {
            favorite: [42, 17, 99],
            status: false,
        };

        const numberToRemove = 17;
        const newState = favoriteReducer(initialState, removeFromFavorite(numberToRemove));

        expect(newState.favorite).toEqual([42, 99]);
    });

    it('should set status to selected', () => {
        const initialState = {
            favorite: [],
            status: false,
        };

        const newState = favoriteReducer(initialState, selected());

        expect(newState.status).toBe(true);
    });

    it('should set status to unselected', () => {
        const initialState = {
            favorite: [],
            status: true,
        };

        const newState = favoriteReducer(initialState, unselected());

        expect(newState.status).toBe(false);
    });

    it('should toggle status', () => {
        const initialState = {
            favorite: [],
            status: false,
        };

        const newState1 = favoriteReducer(initialState, changeStatus());
        expect(newState1.status).toBe(true);

        const newState2 = favoriteReducer(newState1, changeStatus());
        expect(newState2.status).toBe(false);
    });
});
