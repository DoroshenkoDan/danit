import { configureStore } from '@reduxjs/toolkit';
import productsReducer, { fetchProducts } from '../Reducers/productsSlice.js';


describe('productsSlice', () => {
    let store;

    beforeEach(() => {
        store = configureStore({
            reducer: {
                products: productsReducer,
            },
        });
    });

    it('should handle fetchProducts.pending', () => {
        store.dispatch(fetchProducts());

        const { status } = store.getState().products;
        expect(status).toBe('loading');
    });

    it('should handle fetchProducts.fulfilled', async () => {

        const mockApiResponse = ['Product 1', 'Product 2'];
        global.fetch = jest.fn(() =>
            Promise.resolve({
                json: () => Promise.resolve(mockApiResponse),
            }),
        )
        await store.dispatch(fetchProducts());
        const { status, data } = store.getState().products;
        expect(status).toBe('succeeded');
        expect(data).toEqual(['Product 1', 'Product 2']);
    });
});
