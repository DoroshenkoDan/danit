import modalReducer, { modalOpen, modalClose } from '../Reducers/modalReducer.js';

describe('modalReducer', () => {
    it('should open the modal', () => {
        const initialState = {
            status: false,
        };

        const newState = modalReducer(initialState, modalOpen());

        expect(newState.status).toBe(true);
    });

    it('should close the modal', () => {
        const initialState = {
            status: true,
        };

        const newState = modalReducer(initialState, modalClose());

        expect(newState.status).toBe(false);
    });

    it('should not change the status when opening the modal if it is already open', () => {
        const initialState = {
            status: true,
        };

        const newState = modalReducer(initialState, modalOpen());

        expect(newState.status).toBe(true);
    });

    it('should not change the status when closing the modal if it is already closed', () => {
        const initialState = {
            status: false,
        };

        const newState = modalReducer(initialState, modalClose());

        expect(newState.status).toBe(false);
    });
});
