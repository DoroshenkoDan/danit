import { createSlice } from '@reduxjs/toolkit';



const initialState = {
    favorite: [],
    status: false,
}

export const favoriteReducer = createSlice({
    name: 'favorite',
    initialState,
    reducers: {
        addToFavorite(state, action) {
            const numberToAdd = action.payload;
            state.favorite.push(numberToAdd);
        },
        removeFromFavorite(state, action) {
            const idToRemove = action.payload;
            state.favorite = state.favorite.filter(item => item !== idToRemove);
        },
        selected(state) {
            state.status = true;
        },
        unselected(state) {
            state.status = false;
        },
        changeStatus(state) {
            state.status = !state.status;
        },
    }
});

export default favoriteReducer.reducer;
export const { removeFromFavorite, addToFavorite, selected, unselected, changeStatus } = favoriteReducer.actions;
