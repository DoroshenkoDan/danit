
import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from 'react-redux';
import { clearCart, closeCartForm } from '../../store/Reducers/cartReducer';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import "./style.css"
import { AiOutlineClose } from "react-icons/ai"
const validationSchema = Yup.object().shape({
    firstName: Yup.string()
        .required('Please enter your first name.')
        .matches(/^[A-Za-z]+$/, 'Only letters are allowed in the first name.')
        .max(15, 'First name must be 15 characters or less.'),
    lastName: Yup.string()
        .required('Please enter your last name.')
        .matches(/^[A-Za-z]+$/, 'Only letters are allowed in the last name.')
        .max(20, 'Last name must be 20 characters or less.'),
    age: Yup.number()
        .required('Please enter your age.')
        .integer('Age must be an integer.')
        .max(999, 'Age must be 3 digits or less.'),
    address: Yup.string()
        .required('Please enter your address.'),
    number: Yup.string()
        .matches(
            /^\+380\d{9}$/,
            "Invalid phone number (+380XXXXXXXXX)."
        )
        .required("Please enter your phone number."),
});



export default function OrderForm(props) {


    const dispatch = useDispatch();
    const cartList = useSelector(state => state.store.cart.cart)
    const [showSuccessMessage, setShowSuccessMessage] = useState(false);


    useEffect(() => {
        if (showSuccessMessage) {
            const timer = setTimeout(() => {
                setShowSuccessMessage(false);
            }, 5000);
            return () => clearTimeout(timer);
        }
    }, [showSuccessMessage]);


    if (showSuccessMessage) {
        return <>
            <div className="success-message-bg"></div>
            <div className="success-message">
                <p>Congratulations! Your purchase has been successfully completed.</p>
            </div>

        </>
    }


    return (<>
        <div className='from-bg'></div>

        <Formik
            initialValues={{ firstName: '', lastName: '', age: '', address: '', number: '+38' }}
            validationSchema={validationSchema}
            onSubmit={(values, { setSubmitting }) => {
                setShowSuccessMessage(true);


                setTimeout(() => {
                    console.log(JSON.stringify(values, null, 2), cartList);
                    setSubmitting(false);
                    dispatch(clearCart())
                    dispatch(closeCartForm())
                }, 4000);
            }}
        >
            {({ isSubmitting, isValid }) => (
                <Form className='form'>
                    <div className='field-error-wrapper'>
                        <Field className="field" type="text" name="firstName" placeholder="FirstName" />
                        <ErrorMessage className="error-message" name="firstName" component="div" />
                    </div>
                    {/*---*/}
                    <div className='field-error-wrapper'>
                        <Field className="field" type="text" name="lastName" placeholder="lastName" />
                        <ErrorMessage className="error-message" name="lastName" component="div" />
                    </div>
                    {/*---*/}
                    <div className='field-error-wrapper'>
                        <Field className="field" type="number" name="age" placeholder="age" />
                        <ErrorMessage className="error-message" name="age" component="div" />
                    </div>
                    {/*---*/}
                    <div className='field-error-wrapper'>
                        <Field className="field" type="text" name="address" placeholder="address" />
                        <ErrorMessage className="error-message" name="address" component="div" />
                    </div>
                    {/*---*/}
                    <div className='field-error-wrapper'>
                        <Field className="field" type="tel" name="number" placeholder="+380XXXXXXXXX" />
                        <ErrorMessage className="error-message" name="number" component="div" />
                    </div>
                    {/*---*/}
                    <button
                        className="form-submit-btn"
                        type="submit"
                        disabled={isSubmitting || !isValid}
                    >
                        Submit
                    </button>



                    <button onClick={() => { dispatch(closeCartForm()) }} className='close-from'><AiOutlineClose /></button>
                </Form>
            )}
        </Formik>



    </>);
}
