import "./style.css"
import { useContext } from "react";
import { displayProductsContext } from "../../pages/Home";


export default function () {

    const { displayStyle, setDisplayStyle } = useContext(displayProductsContext)

    const handleCheckboxChange = (event) => {
        if (event.target.checked) {
            setDisplayStyle(false)
        } else {
            setDisplayStyle(true)
        }
    };

    return (<>
        <span class="switcher switcher-1">
            <input type="checkbox" id="switcher-1" onChange={handleCheckboxChange} />
            <label for="switcher-1"></label>
        </span>
    </>)

}