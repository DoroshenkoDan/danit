import React, { useEffect, useState, useContext, useCallback } from "react";
import { useSelector, useDispatch } from 'react-redux';
import Card from "../Card";
import "./style.css"
import Modal from "../Modal"
import PreLoader from "../../Components/Preloader";
import { modalOpen, modalClose } from "../../store/Reducers/modalReducer"
import { fetchProducts } from "../../store/Reducers/productsSlice";
import { addToCart, removeFromCart } from "../../store/Reducers/cartReducer";
import { displayProductsContext } from "../../pages/Home";


export default function () {


    const dispatch = useDispatch();
    const list = useSelector(state => state.products.data)
    const [isLoaded, setIsLoaded] = useState(false);

    useEffect(() => {
        setTimeout(() => {
            setIsLoaded(true);
        }, 800);
    }, []);





    let [modalInfo, setModalInfo] = useState([])

    const modalState = useSelector(state => state.modal.status)
    const cartList = useSelector(state => state.store.cart.cart)


    const ModalInfoFunc = useCallback((id) => {
        const obj = list.find(item => item.id === id);
        const objWithCount = { ...obj, count: 1 };
        setModalInfo([objWithCount])
        dispatch(modalOpen())
    }, [list])



    const addToCartFn = useCallback((array) => {

        dispatch(addToCart(array));
        dispatch(modalClose());

    }, [cartList, dispatch]);


    const { displayStyle, setDisplayStyle } = useContext(displayProductsContext)



    if (!isLoaded) {
        return <PreLoader />;
    }

    return (<div className="cards-container" style={{
        flexWrap: displayStyle ? 'wrap' : 'nowrap',
        flexDirection: displayStyle ? 'row' : 'column'
    }}
    >

        {
            list.map((card) => {
                return <Card key={card.id} src={card.src} model={card.model} text={card.text} ModalInfo={() => ModalInfoFunc(card.id)} favId={card.id} />

            })
        }


        {modalState && <Modal model={modalInfo[0].model} saveToCart={() => { addToCartFn(modalInfo) }} closeModal={() => { dispatch(modalClose()) }} />}

    </div >);
}

