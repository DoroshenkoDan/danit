import { render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Modal from './index.js';

describe('Modal Component', () => {
    test('should render correctly', () => {
        const { getByText } = render(<Modal model="Test Model" />);

        expect(getByText('Are you sure you want to add the Test Model to your cart?')).toBeInTheDocument();
        expect(getByText('Yes')).toBeInTheDocument();
        expect(getByText('No')).toBeInTheDocument();
    });

    test('should call saveToCart when "Yes" button is clicked', () => {
        const saveToCartMock = jest.fn();
        const { getByText } = render(<Modal model="Test Model" saveToCart={saveToCartMock} />);

        fireEvent.click(getByText('Yes'));

        expect(saveToCartMock).toHaveBeenCalledTimes(1);
    });

    test('should call closeModal when "No" button is clicked', () => {
        const closeModalMock = jest.fn();
        const { getByText } = render(<Modal model="Test Model" closeModal={closeModalMock} />);

        fireEvent.click(getByText('No'));

        expect(closeModalMock).toHaveBeenCalledTimes(1);
    });
});
