import { BrowserRouter, useRoutes, Navigate } from "react-router-dom";

import Layouts from "../layouts/Layouts";
import Cart from "../pages/Cart";
import Favorite from "../pages/Favorite";
import Home from "../pages/Home";

function Routes() {
    return useRoutes([
        {
            path: '/',
            element: <Layouts />,
            children: [
                {
                    path: '/',
                    element: <Home />,
                },
                {
                    path: 'Favorite/',
                    element: <Favorite />,
                },
                {
                    path: 'Cart/',
                    element: <Cart />
                }
            ]
        }
    ])
}

export default function () {
    return (
        <>
            <BrowserRouter>
                <Routes />
            </BrowserRouter>
        </>
    )
}