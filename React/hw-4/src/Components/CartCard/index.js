import "./style.css"

export default function (props) {
    console.log(process.env.PUBLIC_URL + props.src)
    return (
        <div className="cart-card">
            <div className="cart-card-img-circle">
                <img src={`/${process.env.PUBLIC_URL}${props.src}`} />

            </div>
            <div className="cart-card-description">
                {/*<div className="cart-card-count">
                    <button>-</button>
                    <p>9</p>
                    <button>+</button>
                </div>*/}
                <p>{props.model}</p>
                <p>Price: {props.price}</p>
                {/*<p>Count{props.count}</p>*/}

                <button className="cart-card-order-now">Order Now!</button>
                <button onClick={props.removeFromCart} className="cart-card-remove">Remove</button>
            </div>

        </div>
    )
}


