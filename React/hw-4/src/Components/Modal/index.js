import React, { useState } from "react";
import "./style.css"


export default function (props) {


    return (<>
        <div className="modal-bg"></div>
        <div className="modal">
            <p className="modal-text">Are you sure you want to add the {props.model} to your cart? </p>
            <div className="modal-btn-comtainer">
                <button onClick={props.saveToCart} className="shrink-border">Yes</button>
                <button onClick={props.closeModal} className="shrink-border">No</button>
            </div>
        </div>;
    </>)
}
