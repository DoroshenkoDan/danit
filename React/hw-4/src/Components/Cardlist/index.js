import React, { useEffect, useState, useCallback } from "react";
import { useSelector, useDispatch } from 'react-redux';
import Card from "../Card";
import "./style.css"
import Modal from "../Modal"
import PreLoader from "../../Components/Preloader";
import { modalOpen, modalClose } from "../../store/Reducers/modalReducer"
import { fetchProducts } from "../../store/Reducers/productsSlice";
import { addToCart, removeFromCart } from "../../store/Reducers/cartReducer";

export default function () {
    const [isLoaded, setIsLoaded] = useState(false);
    //------
    const dispatch = useDispatch();
    const list = useSelector(state => state.products.data)


    useEffect(() => {
        dispatch(fetchProducts())
        setTimeout(() => {
            setIsLoaded(true);
        }, 800);
    }, []);




    let [modalInfo, setModalInfo] = useState([])

    const modalState = useSelector(state => state.modal.status)
    const cartList = useSelector(state => state.store.cart.cart)

    const ModalInfoFunc = useCallback((id) => {
        const obj = list.find(item => item.id === id);
        setModalInfo([obj])
        dispatch(modalOpen())
    }, [list])



    const addToCartFn = useCallback((array) => {

        const itemInCart = cartList.find(item => item.id === array[0].id);

        if (itemInCart) {

            alert("Уже есть в корзине");
            dispatch(modalClose());
        } else {

            dispatch(addToCart(array));
            dispatch(modalClose());
        }
    }, [cartList, dispatch]);



    if (!isLoaded) {
        return <PreLoader />;
    }

    return (<div className="cards-container">

        {list.map((card) => {
            return <Card key={card.id} src={card.src} model={card.model} text={card.text} ModalInfo={() => ModalInfoFunc(card.id)} favId={card.id} />

        })}


        {modalState && <Modal model={modalInfo[0].model} saveToCart={() => { addToCartFn(modalInfo) }} closeModal={() => { dispatch(modalClose()) }} />}

    </div>);
}

