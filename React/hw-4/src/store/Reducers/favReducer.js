import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    favorite: []
}

export const favoriteReducer = createSlice({
    name: 'favorite',
    initialState,
    reducers: {
        addToFavorite(state, action) {
            const numberToAdd = action.payload;
            state.favorite.push(numberToAdd);
        },
        removeFromFavorite(state, action) {
            const idToRemove = action.payload;
            state.favorite = state.favorite.filter(item => item !== idToRemove);
        }
    }
});

export default favoriteReducer.reducer;
export const { removeFromFavorite, addToFavorite } = favoriteReducer.actions;
