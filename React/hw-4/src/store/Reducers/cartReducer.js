import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    cart: []
}

export const cartReducer = createSlice({
    name: 'cart',
    initialState,
    reducers: {
        addToCart(state, action) {
            state.cart.push(...action.payload);
        },
        removeFromCart(state, action) {
            const idToRemove = action.payload;
            state.cart = state.cart.filter(item => item.id !== idToRemove);
        }
    }
})


export default cartReducer.reducer
export const { addToCart, removeFromCart } = cartReducer.actions;

