export default async function getProducts() {
    try {
        console.log("Fetch Log");
        const data = await fetch('./products.json');
        const products = await data.json();
        return products;
    } catch (e) {
        console.log(e);
    }
}