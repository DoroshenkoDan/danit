import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from 'react-redux';
import Favorite from "../../Components/Favorite/index.js"
import { fetchProducts } from "../../store/Reducers/productsSlice";
export default function () {

    const [filteredItems, setFilteredItems] = useState([]);



    const dispatch = useDispatch();
    const favoriteList = useSelector(state => state.store.favorite.favorite)
    const list = useSelector(state => state.products.data)


    useEffect(() => {
        dispatch(fetchProducts())
    }, []);



    useEffect(() => {
        if (list) {
            const filtered = list.filter(item => favoriteList.includes(item.id));
            setFilteredItems(filtered);
        }
    }, [list, favoriteList]);



    if (favoriteList.length === 0) {
        return <p>No Items</p>;
    }

    return (
        <>
            {filteredItems.map((card) => {
                return <Favorite src={card.src} model={card.model} text={card.text} price={card.price} favId={card.id} />

            })}
        </>
    );
}
