import CartCard from "../../Components/CartCard";
import "../../Components/CartCard/style.css"
import "./style.css"
import { useSelector, useDispatch } from 'react-redux';
import { removeFromCart } from "../../store/Reducers/cartReducer";
export default function () {

    const dispatch = useDispatch();
    const cartList = useSelector(state => state.store.cart.cart)



    if (cartList.length === 0) {
        return <p>No Items</p>;

    }

    return (
        <div className="container">
            {cartList.map((card) => {
                return <CartCard key={card.id} src={card.src} model={card.model} price={card.price} removeFromCart={() => { dispatch(removeFromCart(card.id)) }} />

            })}
        </div>

    );
}

