import { Component } from "react";
import styles from "./Button.module.scss"

export default class ButtonAddToCart extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        const { text, handleClick } = this.props;
        return (
            <button
                className={styles.btn}
                onClick={handleClick}
            >
                {text}
            </button>
        )
    }
}
