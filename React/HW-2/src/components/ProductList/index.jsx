import React, { PureComponent } from "react";
import "./style.scss";
import Card from "../ProductCard"
import "../ProductCard/style.scss"
import PropTypes from 'prop-types';
class CardsList extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: [],
            ModalAddToCart: false,
        }
    }

    componentDidMount() {
        this.fetchData();
    }



    fetchData = async () => {
        await fetch("./products.json")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result,
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error,
                    });
                }
            );
    }

    render() {
        let { cartCount, favorite } = this.props
        const { items } = this.state
        const array = items.map(item =>
            <Card article={item.article} name={item.name} price={item.price} color={item.color} url={item.url} favorite={favorite} key={item.article} cartCount={cartCount} />
        )
        return (
            <main>

                {array}

            </main>

        )
    }
}

CardsList.propTypes = {
    cartCount: PropTypes.func,
    favorite: PropTypes.func
};


export default CardsList;

