import React, { PureComponent } from "react";
import "./style.scss";
import cross from "./cross.svg"

class ModalAddToCart extends PureComponent {
    render() {
        const { header, text, closeButton, actions, closeModale } = this.props;
        return (
            <div className="modal">
                <div className="modal__close-bg" onClick={closeModale}></div>
                <div className="modal__container">
                    <div className="modal__container__headerModal">
                        <h2 className="modal__container__headerModal__headerTitle">{header}</h2>
                        {closeButton && (
                            <button
                                className="modal__container__headerModal__btnClose"
                                onClick={closeModale}
                            ><img src={cross} alt="" /></button>
                        )}
                    </div>
                    <div className="modal__container__modalBody">{text}</div>
                    <div className="modal__container__footerBtn">{actions}</div>
                </div>
            </div>
        );
    }
}

export default ModalAddToCart;