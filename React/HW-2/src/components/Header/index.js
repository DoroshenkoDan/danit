import { Component } from "react";
import "./style.scss"




export class Header extends Component {
    constructor(props) {
        super(props);
    }
    //, favoriteCount 
    render() {
        let { countGoods, favorities } = this.props
        return (
            <>
                <header>
                    {/*<a className="logo" href=""><img src="./logo.png" /></a>*/}
                    <div className="icon-cart">
                        <div className="cart-line-1" style={{ backgroundColor: "#E5E9EA" }}></div>
                        <div className="cart-line-2" style={{ backgroundColor: "#E5E9EA" }}></div>
                        <div className="cart-line-3" style={{ backgroundColor: "#E5E9EA" }}></div>
                        <div className="cart-wheel" style={{ backgroundColor: "#E5E9EA" }}></div>
                        <p>{countGoods}</p>

                    </div>
                    <p>{favorities}</p>
                    {/*<p>Favorites:{favoriteCount}</p>*/}
                </header>


            </>
        );
    }
}

export default Header