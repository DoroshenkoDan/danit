import React, { PureComponent } from "react";
import "./style.scss";
import ModalAddToCart from "../ModalAddToCart/index.jsx"
import ButtonAddToCart from "../ButtonAddToCart/index.jsx"
import Star from "../FavoriteProduct";
import Header from "../Header";
import PropTypes from 'prop-types';

class Card extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            colorFavorite: "gray",
            ModalAddToCart: false,
        }
    }

    change(key, value) {
        this.setState({ [key]: value })

    }



    favoriteChangeColor = () => {
        const color = this.state.colorFavorite
        if (color === "gray") {
            this.setState({ colorFavorite: "orange" })
        } else {
            this.setState({ colorFavorite: "gray" })
        }
    }


    render() {
        const { article, name, price, color, url, cartCount, favorite } = this.props;
        return (

            <>

                <div className="card" >
                    <div className="card__name-article-wrapper">
                        <Star StarColor={this.state.colorFavorite} handleClick={() => { this.favoriteChangeColor(); favorite(article); }} />
                        <p className="card__name-article-wrapper__name">{name}</p>
                        <p className="card__name-article-wrapper__article">{article}</p>
                    </div>
                    <img className="card__img" src={url} alt="" />
                    <div className="card__color-price-wrapper">
                        <p className="card__color-price-wrapper__price">Price : {price} $</p>
                        <p className="card__color-price-wrapper__color">Color : {color}</p>


                        <ButtonAddToCart
                            text="Add To Cart"
                            handleClick={() => {
                                this.change("ModalAddToCart", true)
                            }} />
                    </div>
                    {this.state.ModalAddToCart && <ModalAddToCart
                        header={`Do you want to add ${name} to cart?`}
                        closeButton={true}
                        closeModale={() => { this.change("ModalAddToCart", false) }}
                        actions={<>
                            <ButtonAddToCart
                                text="YES"
                                handleClick={() => {
                                    this.change("ModalAddToCart", false)
                                    cartCount(article)
                                }}
                            />

                            <ButtonAddToCart
                                text="No"
                                handleClick={() => {
                                    this.change("ModalAddToCart", false)
                                }} />
                        </>}
                    />
                    }
                </div >
            </>
        )
    }
}


Card.propTypes = {
    article: PropTypes.string,
    name: PropTypes.string,
    price: PropTypes.string,
    color: PropTypes.string,
    url: PropTypes.string,
    cartCount: PropTypes.func,
    favorite: PropTypes.func
};

export default Card