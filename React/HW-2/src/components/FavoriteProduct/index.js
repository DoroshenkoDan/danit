import React, { PureComponent } from "react";
import "./style.scss";

class Star extends PureComponent {
    constructor(props) {
        super(props);
    }


    render() {
        const { StarColor, handleClick } = this.props
        return (

            <button onClick={handleClick} style={{ '--star-color': StarColor }} className="five-pointed-star">

            </button>

        )
    }

}

export default Star;