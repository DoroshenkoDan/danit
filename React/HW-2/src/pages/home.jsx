import { Component } from "react";
import CardsList from "../components/ProductList"
import "./style.scss"
import "./reset.min.css"
import Header from "../components/Header"


export class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cartGoodsCount: 0,
            goodsArr: [],
            favoritesArr: [],
        }
        this.changeCatrGoodsCount = this.changeCatrGoodsCount.bind(this)
        this.addToFavorite = this.addToFavorite.bind(this)
    }

    componentDidMount() {
        if (JSON.parse(localStorage.getItem("goodsArr"))) {
            let valueString = JSON.parse(localStorage.getItem("goodsArr"))
            let arr = [...valueString]
            return this.setState({ goodsArr: arr })
        }
    }


    addToFavorite(id) {
        let arr = [...this.state.favoritesArr]
        if (arr.includes(id)) {
            const filteredArray = arr.filter((item) => item !== id);
            localStorage.setItem("Favorites", JSON.stringify(filteredArray))
            return this.setState({ favoritesArr: filteredArray })
                ;
        } else {
            let arr = [...this.state.favoritesArr, id]
            localStorage.setItem("Favorites", JSON.stringify(arr))
            return this.setState({ favoritesArr: arr })

        }

    }


    changeCatrGoodsCount(id) {
        let arr = [...this.state.goodsArr, id]
        localStorage.setItem("goodsArr", JSON.stringify(arr))
        return this.setState({ goodsArr: arr })
    }

    render() {
        return (
            <>
                <Header countGoods={this.state.goodsArr.length} favorities={this.state.favoritesArr.length} />
                <CardsList cartCount={this.changeCatrGoodsCount} favorite={this.addToFavorite} />
            </>
        );
    }
}

//cartCount={this.changeCatrGoodsCount}