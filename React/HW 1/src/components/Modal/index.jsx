import React, { PureComponent } from "react";
import styles from "./Modal.module.scss";
import cross from "./cross.svg"
class Modal extends PureComponent {
    render() {
        const { header, text, closeButton, actions, closeModale } = this.props;
        return (
            <div className={styles.modal}>
                <div className={styles.modal__bgColorModal} onClick={closeModale}></div>
                <div className={styles.modal__container}>
                    <div className={styles.modal__container__headerModal}>
                        <h2 className={styles.modal__container__headerModal__headerTitle}>{header}</h2>
                        {closeButton && (
                            <button
                                className={styles.modal__container__headerModal__btnClose}
                                onClick={closeModale}
                            ><img src={cross} alt="" /></button>
                        )}
                    </div>
                    <div className={styles.modal__container__modalBody}>{text}</div>
                    <div className={styles.modal__container__footerBtn}>{actions}</div>
                </div>
            </div>
        );
    }
}

export default Modal;
