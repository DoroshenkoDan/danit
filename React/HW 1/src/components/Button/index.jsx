import { Component } from "react";
import styles from "./Button.module.scss"

export default class Button extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        const { text, handleClick, backgroundColor, classForBtn } = this.props;
        return (
            <button
                className={`${classForBtn}`}
                style={{ backgroundColor: backgroundColor }}
                onClick={handleClick}
            >
                {text}
            </button>
        )
    }
}
