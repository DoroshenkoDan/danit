import { Component } from "react";
import Button from "../components/Button";
import Modal from "../components/Modal"
import stylesBtn from "../components/Button/Button.module.scss"
import stylesModal from "../components/Modal/Modal.module.scss"
import SecondModal from "../components/SecondModal/index.jsx"
import "./style.scss"
export class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {

            fisrtModalState: false,
            secondModalState: false
        }
    }



    change(key, value) {
        this.setState({ [key]: value })

    }

    render() {


        return (
            <>
                <div className="home-container">
                    <Button
                        backgroundColor="rgba(179, 56, 44, 1)"
                        classForBtn={`${stylesBtn.btn}`}
                        text="Open first modal"
                        handleClick={() => {
                            this.change("fisrtModalState", true)
                        }} />

                    <Button
                        backgroundColor="rgb(80, 12, 145)"
                        classForBtn={`${stylesBtn.btn}`}
                        text="Open second modal"
                        handleClick={() => {
                            this.change("secondModalState", true)
                        }} />
                </div>
                {this.state.fisrtModalState && <Modal
                    header="Do you want to delete this file?"
                    text="Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?"
                    closeButton={true}
                    closeModale={() => { this.change("fisrtModalState", false) }}
                    actions={<>
                        <Button
                            classForBtn={`${stylesModal.modal__container__footerBtn__button}`}
                            backgroundColor="rgba(179, 56, 44, 1)"
                            text="OK"
                            handleClick={() => {
                                this.change("fisrtModalState", false)
                            }}
                        />

                        <Button
                            classForBtn={`${stylesModal.modal__container__footerBtn__button}`}
                            backgroundColor="rgba(179, 56, 44, 1)"
                            text="Cancel"
                            handleClick={() => {
                                this.change("fisrtModalState", false)
                            }} />
                    </>}
                />}

                {this.state.secondModalState && <SecondModal
                    header="Do you want to save this file?"
                    text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ornare, justo a fringilla fermentum, nisi urna?"
                    closeButton={true}
                    closeModale={() => { this.change("secondModalState", false) }}
                    actions={<>
                        <Button
                            classForBtn={`${stylesModal.modal__container__footerBtn__button}`}
                            backgroundColor="rgb(80, 12, 145)"
                            text="OK"
                            handleClick={() => {
                                this.change("secondModalState", false)
                            }}
                        />

                        <Button
                            classForBtn={`${stylesModal.modal__container__footerBtn__button}`}
                            backgroundColor="rgb(80, 12, 145)"
                            text="Cancel"
                            handleClick={() => {
                                this.change("secondModalState", false)
                            }} />
                    </>}
                />}



            </>
        );
    }
}
