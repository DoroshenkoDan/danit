import {useSelector, useDispatch} from "react-redux";

export default function ReduxPage() {

    const counter = useSelector(state => state.counterReducer.counter);
    //const counter = useSelector(function (state) {return  state.counter;});

    const dispatch = useDispatch();


    return (
        <>
            <h1>Redux example 1</h1>

            <div>Counter: {counter}</div>

            <div>
                <button onClick={() => dispatch({type: "INCREMENT"})}>Increment 1 time</button>
            </div>

            <div>
                <button onClick={() => dispatch({type: "INCREMENT", payload: {times: 10}})}>Increment 10 times</button>
            </div>

            <div>
                <button onClick={() => dispatch({type: "DECREMENT", payload: {times: 1}})}>Decrement 1 time</button>
            </div>
            <div>
                <button onClick={() => dispatch({type: "RESET"})}>Reset</button>
            </div>
        </>
    )

}