import {useSelector, useDispatch} from "react-redux";

import * as counterSlice from "../../store/reducers/counterSlice";

export default function ReduxPage3() {

    const counter = useSelector(state => state.counterSlice.counter);

    const dispatch = useDispatch();

    console.log(counterSlice.increment());

    return (
        <>
            <h1>Redux example 3</h1>

            <div>Counter: {counter}</div>

            <div>
                <button onClick={() => dispatch({type:'counterSlice/increment', action: {times: 1} })}>Increment 1 time</button>
            </div>

            <div>
                <button onClick={() => dispatch(counterSlice.increment({times: 1}))}>Increment 1 time</button>
            </div>

            <div>
                <button onClick={() => dispatch(counterSlice.increment({times: 10}))}>Increment 10 times</button>
            </div>

            <div>
                <button onClick={() => dispatch(counterSlice.decrement({times: 1}))}>Decrement 1 time</button>
            </div>
            <div>
                <button onClick={() => dispatch(counterSlice.reset())}>Reset</button>
            </div>
        </>
    )

}