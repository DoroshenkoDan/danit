import {useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import Loader from "../components/Loader";

export default function () {

    const {id} = useParams();

    const [data, setData] = useState({});

    useEffect(() => {
        fetch('https://my-json-server.typicode.com/typicode/demo/posts/' + id)
            .then(res => res.json())
            .then(data => setData(data));
    }, []);

    return (
        <>
            { data.title ?
                (<>
                    <h1>№{id} - {data.title}</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad fugiat itaque quidem quisquam voluptatem. A ad delectus dignissimos ea ex, excepturi fuga fugit minus, officia optio perferendis quae quidem quis.</p>
                </>) : <Loader />
            }
        </>
    )
}