import {useSelector, useDispatch} from "react-redux";
import {addItemAction} from "../../store/reducers/listReducer";

export default function ReduxPage2() {

    const list = useSelector(state => state.listReducer.listItems);

    const dispatch = useDispatch();


    return (
        <>
            <h1>Redux example 2</h1>

            <ul>{list.map((item, index) => <li key={index}>{item}</li>)}</ul>

            <div>
                <button onClick={() => dispatch(addItemAction())}>Add item</button>
            </div>

            <input type="text" className='input_filed' />
        </>
    )

}