import {NavLink} from "react-router-dom";
import {useEffect, useState} from "react";
import Loader from "../components/Loader";

export default function () {

    const [posts, setPosts] = useState([]);

    useEffect(() => {
        fetch('https://my-json-server.typicode.com/typicode/demo/posts')
            .then(res => res.json())
            .then(posts => setPosts(posts));
    }, []);

    const content = posts.length ? posts.map(post => {
                const url = `/blog/post/${post.id}/`;
                return <li key={post.id}><NavLink to={url}>{post.title}</NavLink></li>
            }) : <Loader/>;

    return (
        <>
            <h1>Blog main page</h1>
            <ul className='list-unstyled'>
                {content}
            </ul>
        </>
    )
}