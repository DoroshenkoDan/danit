import {useParams} from "react-router-dom";

import {useLoaderData} from "react-router-dom";

export default function () {

    const {id} = useParams();

    const data = useLoaderData();

    return (
        <>
            <h1>№{id} - {data.title}</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad fugiat itaque quidem quisquam voluptatem. A ad delectus dignissimos ea ex, excepturi fuga fugit minus, officia optio perferendis quae quidem quis.</p>
        </>
    )
}