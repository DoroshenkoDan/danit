import {Provider} from "react-redux";
import {configureStore, combineReducers} from "@reduxjs/toolkit";


//Імпорт модулів, необхідних для зберігання стейджу між перезавантаженнями
import storage from 'redux-persist/lib/storage'
import {persistReducer, persistStore} from "redux-persist";
import {PersistGate} from "redux-persist/integration/react";

//Импорт редюсерів
import counterReducer from "./reducers/counterReducer";
import listReducer from "./reducers/listReducer";
import counterSlice from "./reducers/counterSlice";

//Об'єднання редюсерів
const allReducers = combineReducers({
    counterReducer, listReducer, counterSlice
});

const persistedReducers = persistReducer({key: 'root', storage}, allReducers);

const store = configureStore({
    reducer: persistedReducers
});

const persistedStore = persistStore(store);

export default function Store(props) {

    return (
        <Provider store={store}>
            <PersistGate persistor={persistedStore}>
                {props.children}
            </PersistGate>
        </Provider>
    )
}