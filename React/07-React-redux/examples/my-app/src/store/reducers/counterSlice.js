import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    counter: 0,
};

const counterSlice = createSlice({
    name: 'counterSlice',
    initialState,
    reducers: {
        increment(state, action) {
            state.counter += action && action.payload && action.payload.times || 1;
        },
        decrement(state, action) {
            state.counter -= action && action.payload && action.payload.times || 1;
        },
        reset(state) {
            state.counter = initialState.counter;
        }
    }
});

export const {increment, decrement, reset} = counterSlice.actions;
export default counterSlice.reducer