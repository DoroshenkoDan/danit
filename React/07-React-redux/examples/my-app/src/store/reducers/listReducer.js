const initialState = {
    listItems: [
        'item 1', 'item 2', 'item 3',
    ]
}

export default function listReducer(state = initialState, action) {
    switch (action.type) {
        case 'LIST_ADD':
            return {...state, listItems: [...state.listItems, action.payload]};
        case 'LIST_RESET':
            return {...state, listItems: initialState.listItems}
        case 'LIST_ERASE':
            return {...state, listItems: []}
        default:
            return state
    }
}

export const addItemAction = () => {
    const textToAdd = document.querySelector('.input_filed').value;
    document.querySelector('.input_filed').value = '';
    return {type: 'LIST_ADD', payload: textToAdd};
}