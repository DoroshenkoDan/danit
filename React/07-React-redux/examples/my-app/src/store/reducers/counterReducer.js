const initialState = {
    counter: 0,
};

export default function counterReducer(state = initialState, action) {

    const times = action.payload && action.payload.times || 1;

    switch (action.type) {
        case 'INCREMENT':
            return {...state, counter: state.counter + times};
        case 'DECREMENT':
            return {...state, counter: state.counter - times};
        case 'RESET':
            return {...state, counter: 0};
        default:
            return state;
    }

}