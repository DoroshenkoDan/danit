import {Outlet, NavLink} from "react-router-dom";

export default function Layout() {
    return (
        <>
            <header>
                <nav className="navbar navbar-expand-lg bg-light">
                    <div className="container-fluid">
                        <a className="navbar-brand" href="#">Logo</a>
                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                                data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                                aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarNav">
                            <ul className="navbar-nav">
                                <li className="nav-item">
                                    <NavLink className={({isActive})=> isActive ? 'nav-link active' : 'nav-link' } aria-current="page" to="/">Home</NavLink>
                                </li>
                                <li className="nav-item dropdown">
                                    <a className="nav-link dropdown-toggle" href="#" role="button"
                                       data-bs-toggle="dropdown" aria-expanded="false">
                                        Redux
                                    </a>
                                    <ul className="dropdown-menu">
                                        <li><NavLink className="dropdown-item" to="/redux/">Example 1</NavLink></li>
                                        <li><NavLink className="dropdown-item" to="/redux2/">Example 2</NavLink></li>
                                        <li><NavLink className="dropdown-item" to="/redux3/">Example 3</NavLink></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </header>
            <main className='container'>
                <Outlet />
            </main>
            <footer>

            </footer>
        </>
    )
}