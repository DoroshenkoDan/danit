import {Outlet, NavLink} from "react-router-dom";

export default function() {
    return (
        <>
            <header>
                <h1>This is Blog</h1>
            </header>
            <main className='container'>
                <Outlet />
            </main>
            <footer>

            </footer>
        </>
    )
}