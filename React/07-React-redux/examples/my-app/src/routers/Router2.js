import {BrowserRouter, useRoutes} from "react-router-dom";

import Layout from "../layouts/Layout";
import BlogLayout from "../layouts/BlogLayout"

import AboutUs from "../pages/AboutUs";
import Contacts from "../pages/Contacts";
import Home from "../pages/Home";
import Blog from "../pages/Blog";
import BlogPost from "../pages/BlogPost";
import PageNotFound from "../pages/PageNotFound";
import ReduxPage from "../pages/ReduxPage";
import ReduxPage2 from "../pages/ReduxPage2";
import ReduxPage3 from "../pages/ReduxPage3";

function Routes() {
    return useRoutes([
        {
            path: '/',
            element: <Layout />,
            children: [
                {path: '', element: <Home /> },
                {path: 'about/', element: <AboutUs /> },
                {path: 'contacts/', element: <Contacts /> },
                {path: 'redux/', element: <ReduxPage />},
                {path: 'redux2/', element: <ReduxPage2 />},
                {path: 'redux3/', element: <ReduxPage3 />},
                {path: '*', element: <PageNotFound />},
                {
                    path: '/blog/',
                    element: <BlogLayout />,
                    children: [
                        {path: '', element: <Blog /> },
                        {
                            path: 'post/:id/', element: <BlogPost />,
                        },
                    ]
                }
            ]
        },

    ]);
}

export default function () {
    return (
        <>
            <BrowserRouter>
                <Routes />
            </BrowserRouter>
        </>
    )
}