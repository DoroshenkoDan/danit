import {createBrowserRouter, RouterProvider} from "react-router-dom";

import Layout from "../layouts/Layout";
import BlogLayout from "../layouts/BlogLayout"

import AboutUs from "../pages/AboutUs";
import Contacts from "../pages/Contacts";
import Home from "../pages/Home";
import Blog from "../pages/Blog";
import PageNotFound from "../pages/PageNotFound";
import Loader from "../components/Loader";

function Routes() {
    return createBrowserRouter([
        {
            path: '/',
            element: <Layout />,
            children: [
                {path: '', element: <Home /> },
                {path: 'about/', element: <AboutUs /> },
                {path: 'contacts/', element: <Contacts /> },
                {path: '*', element: <PageNotFound />},
                {
                    path: '/blog/',
                    element: <BlogLayout />,
                    children: [
                        {path: '', element: <Blog /> },
                        {
                            path: 'post/:id/',
                            element: <BlogPostWithLoader />,
                            loader: ({params}) => {
                                return fetch('https://my-json-server.typicode.com/typicode/demo/posts/' + params.id)
                            },
                        },
                    ]
                }
            ]
        },
    ]);
}

export default function () {
    return (
        <>
            <RouterProvider router={Routes()} fallbackElement={<Loader />} />
        </>
    )
}