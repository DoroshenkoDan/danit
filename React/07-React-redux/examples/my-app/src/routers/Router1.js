import {BrowserRouter, Routes, Route} from "react-router-dom";

import Layout from "../layouts/Layout";
import BlogLayout from "../layouts/BlogLayout"

import AboutUs from "../pages/AboutUs";
import Contacts from "../pages/Contacts";
import Home from "../pages/Home";
import Blog from "../pages/Blog";
import BlogPost from "../pages/BlogPost";
import PageNotFound from "../pages/PageNotFound";
export default function() {
    return (
        <>
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<Layout />}>
                        <Route index element={<Home />}></Route>
                        <Route path="about/" element={<AboutUs />}></Route>
                        <Route path="contacts/" element={<Contacts />}></Route>
                        <Route path="blog/" element={<BlogLayout />}>
                            <Route index element={<Blog />}></Route>
                            <Route path="post/" element={<BlogPost />}></Route>
                        </Route>
                        <Route path="*" element={<PageNotFound />}></Route>
                    </Route>
                </Routes>
            </BrowserRouter>
        </>
    )
}