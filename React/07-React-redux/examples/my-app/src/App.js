
import './App.css';
import Router from "./routers/Router2";
import Store from "./store";


function App() {

  return (
    <Store>
      <Router />
    </Store>
  );
}

export default App;
