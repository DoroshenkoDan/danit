const gulp = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const del = require("del");
const cleanCSS = require("gulp-clean-css");
const autoprefixer = require("gulp-autoprefixer");
const minifyjs = require("gulp-js-minify");
const imagemin = require("gulp-imagemin");
const concat = require("gulp-concat");
const fileinclude = require("gulp-file-include");
const browserSync = require("browser-sync").create();
const reload = browserSync.reload;

gulp.task("prepare-html", (cb) => {
    gulp.src("src/*.html").pipe(fileinclude()).pipe(gulp.dest("dist"));
    cb();
});

gulp.task("prepare-img", (cb) => {
    gulp.src("src/img/**").pipe(imagemin()).pipe(gulp.dest("dist/img/"));
    cb()
})

gulp.task("prepare-css", (cb) => {
    gulp
        .src("./src/scss/**/*.scss")
        .pipe(sass().on("error", sass.logError))
        .pipe(
            autoprefixer({
                cascade: false,
            })
        )
        .pipe(cleanCSS({ compatibility: "ie8" }))
        .pipe(gulp.dest("./dist/css/"));

    cb();
});
gulp.task("prepare-js", (cb) => {
    gulp
        .src("src/js/*.js")
        .pipe(concat("bundle.js"))
        //.pipe(minifyjs())
        .pipe(gulp.dest("dist"));

    cb();
});

exports.watch = function watch() {
    gulp.watch("./src/scss/**/*.scss", gulp.series("prepare-css"))
        .pipe(gulp.watch("./src/html/**/*.html", gulp.series("prepare-html")))
        .pipe(gulp.watch("./src/js/**/*.js", gulp.series("prepare-js")));
};

gulp.task("clean", (cb) => {
    del("dist");
    cb();
});

gulp.task("serve", function () {
    browserSync.init({
        server: "./dist",
    });

    gulp.watch("./src/**/**", gulp.parallel("prepare-css", "prepare-html", "prepare-img", "prepare-js"))
    gulp.watch("dist").on("change", reload);
    // gulp.watch("./src/scss/**/*.scss").on("change", reload);
});

console.log(12345);

console.log(678890);

exports.build = gulp.parallel("prepare-html", "prepare-css", "prepare-js", "prepare-img", "serve");
exports.clean = gulp.series("clean");
exports.serve = gulp.series("serve");
