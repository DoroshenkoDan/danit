// -------------1------------
let containerForTagP = document.querySelectorAll("p");
containerForTagP.forEach((p) => (p.style.background = "#ff0000"));

// -------------2------------
let optionsListEl = document.querySelector("#optionsList");
console.log(optionsListEl);
optionsListEl.childNodes.forEach((e) => console.log(e.nodeType));
optionsListEl.childNodes.forEach((e) => console.log(e.nodeName));

// -------------3------------
let testParagraph = document.querySelector("#testParagraph");
testParagraph.textContent = "This is a paragraph";
console.log(testParagraph);

// -------------4-5------------
let mainHeader = document.querySelector(".main-header");
console.log(mainHeader.childNodes);
let mainHeaderChilds = mainHeader.childNodes;
mainHeaderChilds.forEach((element) => element.classList?.add("nav-item"));
console.log(mainHeaderChilds);

// -------------6------------
let sectionTitleElems = document.querySelectorAll(".section-title");
sectionTitleElems.forEach((element) =>
  element.classList?.remove("section-title")
);
