let usernumber = +prompt('Enter Number')

while (usernumber == '' || isNaN(usernumber)) {
    usernumber = prompt("Enter your Number again", usernumber);
  }

  if (usernumber < 5) {
    console.log("Sorry, no numbers");
  } else {
    for (i = 0; i <= usernumber; i++) {
      if (i % 5 === 0) {
        console.log(i);
      }
    }
  }
  