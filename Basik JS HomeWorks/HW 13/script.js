let stopBtn = document.getElementById("stopBtn");
let startBtn = document.getElementById("startBtn");
let photoPack = document.querySelectorAll(".image-to-show");
let intervalFS;
let current = 0;

function showerPH() {
  intervalFS = setInterval(function () {
    photoPack[current].classList.add("hidden");
    current = (current + 1) % photoPack.length;
    photoPack[current].classList.remove("hidden");
  }, 3000);
}

showerPH();

stopBtn.addEventListener("click", () => {
  clearInterval(intervalFS);
});
startBtn.addEventListener("click", function () {
  showerPH();
});

let timerSeconds = document.querySelector("#timerSec");
let idInterval;

function secondsTimerFunc() {
  let x = 3;
  timerSeconds.innerText = x;
  idInterval = setInterval(function () {
    if (x == 1) {
      x = 3;
    } else {
      x -= 1;
    }

    timerSeconds.innerText = x;
  }, 1000);
}

secondsTimerFunc();
