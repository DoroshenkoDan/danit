function createNewUser() {
  let userName = prompt("Enter your Name");
  let userLastName = prompt("Enter your LastName");
  let userBirthday = prompt("Enter your Birthday");
  let curYear = new Date().getFullYear();
  let newUser = {
    firstName: userName,
    lastName: userLastName,
    birthday: userBirthday,
    getAge() {
      let birthdayYear = this.birthday.split(".");
      let age = curYear - birthdayYear[2];
      return console.log("Age: " + age);
    },
    getLogin() {
      let login = this.firstName[0] + this.lastName;
      return console.log("Login: " + login.toLowerCase());
    },
    getPassword() {
      let birthdayYear = this.birthday.split(".");
      let getPassword = this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + birthdayYear[2];
      return console.log('Password: ' + getPassword);
    },
  };
  newUser.getLogin();
  newUser.getAge();
  newUser.getPassword()
}

createNewUser();
