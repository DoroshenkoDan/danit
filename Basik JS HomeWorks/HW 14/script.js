let btn = document.querySelector("#btn");
let changeText = document.querySelectorAll(".lightText");
let changeBack = document.querySelectorAll(".lightBack");
let localStrgKey = localStorage.getItem("side");

// ----------- Cheker of LocalStorage------------------
if (localStrgKey == "dark") {
  changeTextAndBackground();
} else {
  console.log("dont need");
}

// ----------- listener on Button for changing theme------------------
btn.addEventListener("click", function () {
  if (localStrgKey == "light") {
    localStorage.setItem("side", "dark");
    changeTextAndBackground();
  }
});
btn.addEventListener("click", function () {
  if (localStrgKey == "dark") {
    localStorage.setItem("side", "light");
    changeTextAndBackground();
  }
});

// ----------- TEST function for change theme on "Dark"/"Light"------------------
function changeTextAndBackground() {
  changeText.forEach((elem) => {
    elem.classList.toggle("lightText");
    elem.classList.toggle("darkText");
  });
  changeBack.forEach((elem) => {
    elem.classList.toggle("lightBack");
    elem.classList.toggle("darkBack");
  });
  document.body.classList.toggle("lightImgBack");
  document.body.classList.toggle("darkImgBack");
}

// ----------- function for change theme on "darkSide"------------------
// function changeTextBackgroundDark() {
//   changeText.forEach((elem) => {
//     elem.classList.remove("lightText");
//     elem.classList.add("darkText");
//   });
//   changeBack.forEach((elem) => {
//     elem.classList.remove("lightBack");
//     elem.classList.add("darkBack");
//   });
//   document.body.classList.remove("lightImgBack");
//   document.body.classList.add("darkImgBack");
// }

// ----------- function for change theme on "lightSide"------------------

// function changeTextBackgroundLight() {
//   changeText.forEach((elem) => {
//     elem.classList.remove("darkText");
//     elem.classList.add("lightText");
//   });
//   changeBack.forEach((elem) => {
//     elem.classList.remove("darkText");
//     elem.classList.add("lightText");
//   });
//   document.body.classList.remove("darkImgBack");
//   document.body.classList.add("lightImgBack");
// }
