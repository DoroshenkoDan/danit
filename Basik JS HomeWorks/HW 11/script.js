let faEye = document.querySelectorAll(".fas");
let input = document.querySelectorAll("input");
let submitBtn = document.querySelector("button");

faEye[0].addEventListener("click", function () {
  if (faEye[0].classList.contains("fa-eye")) {
    faEye[0].classList.remove("fa-eye");
    faEye[0].classList.add("fa-eye-slash");
  } else {
    faEye[0].classList.remove("fa-eye-slash");
    faEye[0].classList.add("fa-eye");
  }
  if (input[0].type == "password") {
    input[0].type = "text";
  } else {
    input[0].type = "password";
  }
});

faEye[1].addEventListener("click", function () {
  if (faEye[1].classList.contains("fa-eye")) {
    faEye[1].classList.remove("fa-eye");
    faEye[1].classList.add("fa-eye-slash");
  } else {
    faEye[1].classList.remove("fa-eye-slash");
    faEye[1].classList.add("fa-eye");
  }
  if (input[1].type == "password") {
    input[1].type = "text";
  } else {
    input[1].type = "password";
  }
});

let textUnderSecondInput = document.createElement("p");
textUnderSecondInput.innerText = "Потрібно ввести однакові значення";
textUnderSecondInput.style.color = "red";

submitBtn.addEventListener("click", function () {
  textUnderSecondInput.style.display = "none";
  if (input[0].value === input[1].value) {
    alert("You are Welcome!)");
  } else {
    textUnderSecondInput.style.display = "contents";
    faEye[1].after(textUnderSecondInput);
  }
});
