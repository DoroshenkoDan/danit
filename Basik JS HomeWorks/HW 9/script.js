let arrFirst = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
let arrSecond = ["1", "2", "3", "sea", "user", 23];

function addListOnPage(arr, arg = document.body) {
  let ul = document.createElement("ul");
  for (let i = 0; i < arr.length; i++) {
    let li = document.createElement("li");
    ul.append(li);

    li.append(arr[i]);
    arg.append(ul);
  }
}
addListOnPage(arrFirst);

addListOnPage(arrSecond);
