class Emloyee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    //---
    getName() {
        return this.name
    }
    getAge() {
        return this.age
    }
    getSalary() {
        return this.salary
    }
    //---
    setName(name) {
        return this.name = name
    }
    setAge(age) {
        return this.age = age
    }
    setSalary(salary) {
        return this.salary = salary
    }
}

class Programmer extends Emloyee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    getSalary() {
        return this.salary * 3;
    }


}


let prog1 = new Programmer("Dima", 21, 24900, 'english, italian, ukranian')
let prog2 = new Programmer("Danya", 19, 28300, 'ukranian, english, polish')

console.log(prog1);
console.log(prog2);

