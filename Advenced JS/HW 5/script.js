async function getData(data) {
    return await fetch(`https://ajax.test-danit.com/api/json/${data}`)
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            return data;
        })
}


async function pendingPost() {
    const users = await getData("users")
    const posts = await getData("posts")
    let n = 0
    let containersArr = []
    users.forEach(users => {
        n++
        let container = new Post(users.username, users.name, users.id, users.email, posts[n].title, posts[n].body).printPost()
        containersArr.push(container);
    })

    await new Promise(resolve => setTimeout(resolve, 0));
}

pendingPost()


class Post {
    constructor(username, name, id, email, title, body) {
        this.username = username;
        this.name = name;
        this.id = id;
        this.email = email;
        this.title = title
        this.body = body
    }

    printPost() {
        let container = document.createElement('div');
        container.classList.add("container", "post")
        container.setAttribute('id', this.id);
        let containerHtml = `<button class="btn-remove btn-remove${this.id}"></button>
        <p class="name">${this.username}</p>
        <div class="block-name">
        <p class="surname">${this.name}</p>
        <p class="mail">${this.email}</p>
        </div>
        <p class="title">${this.title}</p >
        <p class="maintext">${this.body}</p>`;
        container.innerHTML = containerHtml;
        document.body.appendChild(container);
        let btnRemove = document.querySelector(`.btn-remove${this.id}`)
        btnRemove.onclick = this.deletePost;
        return container
    }
    deletePost() {
        console.log("test", this)
        let parent = this.parentElement
        fetch(`https://ajax.test-danit.com/api/json/posts/${parent.id}`, {
            method: 'DELETE',
        }).then(response => {
            if (response.status == 200) {
                console.log(response.status);
                parent.remove()
            } else {
                throw new Error('server did not answer')
            }
        })
    }
}