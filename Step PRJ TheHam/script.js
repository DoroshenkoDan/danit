// ------------------OurService------------------

let ourServiceButtons = document.querySelectorAll(
    ".our-services-buttons-container > button"
);

let restanglesOfOurService = document.querySelectorAll(
    ".our-services-buttons-container>button>img"
);

let ourServiceContent = document.querySelectorAll(".gog");
// ---1---
ourServiceButtons[0].addEventListener("click", function () {
    ourServiceButtons.forEach((e) =>
        e.classList.remove("focus-buttons-our-service")
    );
    ourServiceButtons.forEach((e) =>
        e.classList.add("none-focus-buttons-our-service")
    );
    restanglesOfOurService.forEach((e) => e.classList.add("displayNone"));
    ourServiceContent.forEach((e) => e.classList.remove("our-services-content"));
    ourServiceContent.forEach((e) => e.classList.add("displayNone"));
    ourServiceContent[0].classList.add("our-services-content");
    ourServiceContent[0].classList.remove("displayNone");
    ourServiceButtons[0].classList.add("focus-buttons-our-service");
    restanglesOfOurService[0].classList.remove("displayNone");
});

// ---2---
ourServiceButtons[1].addEventListener("click", function () {
    ourServiceButtons.forEach((e) =>
        e.classList.remove("focus-buttons-our-service")
    );
    ourServiceButtons.forEach((e) =>
        e.classList.add("none-focus-buttons-our-service")
    );
    restanglesOfOurService.forEach((e) => e.classList.add("displayNone"));
    ourServiceContent.forEach((e) => e.classList.remove("our-services-content"));
    ourServiceContent.forEach((e) => e.classList.add("displayNone"));
    ourServiceContent[1].classList.add("our-services-content");
    ourServiceContent[1].classList.remove("displayNone");
    ourServiceButtons[1].classList.add("focus-buttons-our-service");
    restanglesOfOurService[1].classList.remove("displayNone");
});

// ---3---
ourServiceButtons[2].addEventListener("click", function () {
    ourServiceButtons.forEach((e) =>
        e.classList.remove("focus-buttons-our-service")
    );
    ourServiceButtons.forEach((e) =>
        e.classList.add("none-focus-buttons-our-service")
    );
    restanglesOfOurService.forEach((e) => e.classList.add("displayNone"));
    ourServiceContent.forEach((e) => e.classList.remove("our-services-content"));
    ourServiceContent.forEach((e) => e.classList.add("displayNone"));
    ourServiceContent[2].classList.add("our-services-content");
    ourServiceContent[2].classList.remove("displayNone");
    ourServiceButtons[2].classList.add("focus-buttons-our-service");
    restanglesOfOurService[2].classList.remove("displayNone");
});

// ---4---
ourServiceButtons[3].addEventListener("click", function () {
    ourServiceButtons.forEach((e) =>
        e.classList.remove("focus-buttons-our-service")
    );
    ourServiceButtons.forEach((e) =>
        e.classList.add("none-focus-buttons-our-service")
    );
    restanglesOfOurService.forEach((e) => e.classList.add("displayNone"));
    ourServiceContent.forEach((e) => e.classList.remove("our-services-content"));
    ourServiceContent.forEach((e) => e.classList.add("displayNone"));
    ourServiceContent[3].classList.add("our-services-content");
    ourServiceContent[3].classList.remove("displayNone");
    ourServiceButtons[3].classList.add("focus-buttons-our-service");
    restanglesOfOurService[3].classList.remove("displayNone");
});
// ---5---
ourServiceButtons[4].addEventListener("click", function () {
    ourServiceButtons.forEach((e) =>
        e.classList.remove("focus-buttons-our-service")
    );
    ourServiceButtons.forEach((e) =>
        e.classList.add("none-focus-buttons-our-service")
    );
    restanglesOfOurService.forEach((e) => e.classList.add("displayNone"));
    ourServiceContent.forEach((e) => e.classList.remove("our-services-content"));
    ourServiceContent.forEach((e) => e.classList.add("displayNone"));
    ourServiceContent[4].classList.add("our-services-content");
    ourServiceContent[4].classList.remove("displayNone");
    ourServiceButtons[4].classList.add("focus-buttons-our-service");
    restanglesOfOurService[4].classList.remove("displayNone");
});

// ---6---
ourServiceButtons[5].addEventListener("click", function () {
    ourServiceButtons.forEach((e) =>
        e.classList.remove("focus-buttons-our-service")
    );
    ourServiceButtons.forEach((e) =>
        e.classList.add("none-focus-buttons-our-service")
    );
    restanglesOfOurService.forEach((e) => e.classList.add("displayNone"));
    ourServiceContent.forEach((e) => e.classList.remove("our-services-content"));
    ourServiceContent.forEach((e) => e.classList.add("displayNone"));
    ourServiceContent[5].classList.add("our-services-content");
    ourServiceContent[5].classList.remove("displayNone");
    ourServiceButtons[5].classList.add("focus-buttons-our-service");
    restanglesOfOurService[5].classList.remove("displayNone");
});

// ------------------AmazingWork------------------

let buttonsAmazingWork = document.querySelectorAll(
    ".our-work-buttons-wrapper > button"
);
let imageAmzingWork = document.querySelectorAll(".img-our-works-box");

let btnLoadMore = document.querySelector(".load-more");
let animationOurWork = document.querySelector(".center");

btnLoadMore.addEventListener("click", function () {
    btnLoadMore.classList.add("act");
    btnLoadMore.classList.add("displayNone");
    animationOurWork.style.display = "flex";
    let interval = setTimeout(function () {
        animationOurWork.style.display = "none";
        imageAmzingWork.forEach((e) => e.classList.remove("displayNone"));
    }, 2000);
});

function amazingWorksOptim(q, w, e, r, t, y) {
    imageAmzingWork.forEach((e) => e.classList.add("displayNone"));
    if (btnLoadMore.classList.contains("act")) {
        imageAmzingWork[q].classList.remove("displayNone");
        imageAmzingWork[w].classList.remove("displayNone");
        imageAmzingWork[e].classList.remove("displayNone");
        imageAmzingWork[r].classList.remove("displayNone");
        imageAmzingWork[t].classList.remove("displayNone");
        imageAmzingWork[y].classList.remove("displayNone");
    } else {
        imageAmzingWork[q].classList.remove("displayNone");
        imageAmzingWork[w].classList.remove("displayNone");
        imageAmzingWork[e].classList.remove("displayNone");
    }
}

function btnAmazingWorkFocus(number) {
    buttonsAmazingWork.forEach((elem) =>
        elem.classList.remove("our-work-button-focused")
    );
    buttonsAmazingWork.forEach((elem) => elem.classList.add("our-work-button"));
    buttonsAmazingWork[number].classList.remove("our-work-button");
    buttonsAmazingWork[number].classList.add("our-work-button-focused");
}

buttonsAmazingWork[0].addEventListener("click", function () {
    if (btnLoadMore.classList.contains("act")) {
        imageAmzingWork.forEach((e) => e.classList.remove("displayNone"));
    } else {
        imageAmzingWork.forEach((e) => e.classList.add("displayNone"));
        imageAmzingWork[0].classList.remove("displayNone");
        imageAmzingWork[1].classList.remove("displayNone");
        imageAmzingWork[2].classList.remove("displayNone");
        imageAmzingWork[3].classList.remove("displayNone");
        imageAmzingWork[4].classList.remove("displayNone");
        imageAmzingWork[5].classList.remove("displayNone");
        imageAmzingWork[6].classList.remove("displayNone");
        imageAmzingWork[7].classList.remove("displayNone");
        imageAmzingWork[8].classList.remove("displayNone");
        imageAmzingWork[9].classList.remove("displayNone");
        imageAmzingWork[10].classList.remove("displayNone");
        imageAmzingWork[11].classList.remove("displayNone");
    }
    btnAmazingWorkFocus(0);
});

buttonsAmazingWork[1].addEventListener("click", function () {
    amazingWorksOptim(0, 4, 8, 12, 16, 20);
    btnAmazingWorkFocus(1);
});

buttonsAmazingWork[2].addEventListener("click", function () {
    btnAmazingWorkFocus(2);
    amazingWorksOptim(2, 6, 10, 14, 18, 22);
});

buttonsAmazingWork[3].addEventListener("click", function () {
    btnAmazingWorkFocus(3);
    amazingWorksOptim(1, 5, 9, 13, 17, 21);
});

buttonsAmazingWork[4].addEventListener("click", function () {
    btnAmazingWorkFocus(4);
    amazingWorksOptim(3, 7, 11, 15, 19, 23);
});
