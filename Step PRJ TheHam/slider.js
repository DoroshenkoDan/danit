const names = ['John Smith', 'Sarah Jones', 'David Miller', 'Emily Anderson'];
const professions = ['Developer', 'Lawyer', 'Engineer', 'Designer'];
const personalities = [
    "Responsible, hardworking, reliable, and detail-oriented. He is also ambitious, creative, and always looking for ways to improve his skills and knowledge. In addition, he is patient, empathetic, and a good listener, making him a great team player and problem solver.",
    "Outgoing, friendly, and approachable. She is also organized, efficient, and able to multitask effectively. She has a positive attitude and is always willing to help others, making her a valuable member of any team. Additionally, she is adaptable, open-minded, and eager to learn new things.",
    "Confident, assertive, and a natural leader. He is able to make tough decisions and take responsibility for his actions. He is also analytical, strategic, and has excellent problem-solving skills. In addition, he is goal-oriented, driven, and not afraid to take risks.",
    "Kind-hearted, compassionate, and understanding. She is patient, a good listener, and able to empathize with others. She is also creative, imaginative, and enjoys expressing herself through art and design. Additionally, she is detail-oriented, meticulous, and strives for perfection in everything she does.",
];
const avatars = document.querySelectorAll(".peoples")


let btnPrev = document.querySelector("#btn1")
let btnNext = document.querySelector("#btn2")
let charPhoto = document.querySelector("#mainPeople")
let charDescription = document.querySelector("#char-descriptions")
let charName = document.querySelector("#char-name")
let charJob = document.querySelector("#char-job")
let step = 0;


function nextPerson(pers, name, profs) {
    if (step >= 3) {
        step = 0
    } else {
        step++
    }

    charDescription.innerHTML = pers[step]
    charName.innerHTML = name[step]
    charJob.innerHTML = profs[step]
    charPhoto.innerHTML = avatars[step].outerHTML
    avatars.forEach(elem => {
        elem.classList.remove("selected-avatar")
    })
    avatars[step].classList.add("selected-avatar")
}
function prevPerson(pers, name, profs) {
    if (step == 0) {
        step = 3
    } else {
        step--
    }

    charDescription.innerHTML = pers[step]
    charName.innerHTML = name[step]
    charJob.innerHTML = profs[step]
    charPhoto.innerHTML = avatars[step].outerHTML
    avatars.forEach(elem => {
        elem.classList.remove("selected-avatar")
    })
    avatars[step].classList.add("selected-avatar")
}

let intervalNextSlide = setInterval(() => {
    nextPerson(personalities, names, professions)
}, 4000)


btnNext.addEventListener("click", () => {
    clearInterval(intervalNextSlide)
    nextPerson(personalities, names, professions)
})
btnPrev.addEventListener("click", () => {
    clearInterval(intervalNextSlide)
    prevPerson(personalities, names, professions)
})
